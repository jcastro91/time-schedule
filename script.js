$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var data = $('form').serializeObject();

$(document).ready(function(){
    var time1 = new Date();
    $("input[name=time]").val(time1);
});

// $(document).ready(function(){
//     var time1 = new Date();
//     $("input[name=time]").val(time1);
//     $('form').submit(function() {
//         var data = $('form').serializeObject();
//         console.log(data);
//         $.ajax({
//             type: "POST",
//             url: 'http://127.0.0.1:8080/send',
//             data: data,
//             success: function(){console.log('hola')},
//         });
//         return false;
//     });
// });

// $(document).ready(function(){
//     $.couch.urlPrefix = "http://localhost:5984";
//     $.couch.info({
//         success: function(data) {
//             console.log(data);
//         }
//     });
//     var time1 = new Date();
//     $("input[name=time]").val(time1);
//     var $db = $.couch.db("time");
//     var doc = $('form').serializeObject();
//     console.log(doc);
//     $('form').submit(function(){
//         console.log(doc);
//         $db.saveDoc(
//             doc,
//             {
//                 success: function(data) {
//                     console.log(data);
//                 },
//                 error: function(status) {
//                     console.log(status);
//                 }
//             }
//         );
//         return false;
//     });
// });



